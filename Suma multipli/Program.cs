﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Suma_multipli
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Numarul n este: ");
            int n = int.Parse(Console.ReadLine());

            Console.WriteLine("Numarul m este: ");
            int m = int.Parse(Console.ReadLine());

            Console.WriteLine("Suma multiplilor este " + SumaMultipli(n, m));

            Console.ReadKey();
        }

        static int SumaMultipli(int x, int y)
        {
            int rez = 0;
            for (int i = 1; i <= y / x; i++)
                rez = rez + i * x;

            return rez;
        }
    }
}
